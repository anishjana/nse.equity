import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: 'https://nse-server.herokuapp.com/' || `http://localhost:5000/`
  })
}
