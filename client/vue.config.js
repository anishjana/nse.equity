/* eslint-disable no-unused-vars */
// eslint-disable-next-line
// var webpack = require('webpack');
const path = require('path')

module.exports = {
  publicPath: '/api/search',
  devServer: {
    proxy: {
      '/': {
        target: 'https://nse-server.herokuapp.com/'
      }
    }
  }
}
